import numpy as np
from sklearn.ensemble import RandomForestClassifier

with open("data/train.npy", "rb") as f:
    train = np.load(f)

with open("data/test.npy", "rb") as f:
    test = np.load(f)

X_train, y_train = train[:,:-1], train[:,-1]
X_test, y_test = test[:,:-1], test[:,-1]

clf = RandomForestClassifier()
clf.fit(X_train, y_train)

y_pred = clf.predict(X_test)

with open("data/prediction.txt", "w") as f:
    np.savetxt(f, y_pred)


