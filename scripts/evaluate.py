import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score

with open("data/test.npy", "rb") as f:
    y_test = np.load(f)[:,-1]
    
y_pred = np.genfromtxt("data/prediction.txt")

accuracy = accuracy_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred, average="macro")

with open("data/eval.txt", "w") as f:
    print("Accuracy:", accuracy, file=f)
    print("F1 score:", f1, file=f)
